# Data Analitics and advanced programming

by **Cesar Uribe**

Year: 2020

This course is about an  introduction to data analysis, where we can learn about previous step needed to apply an
analysis or apply machine learning models. The course consist:

 * Make a review of python
 * Learn about how to consume or extract data from different sources
 * Make a process of an initial data cleaning
 * Review and Learn tools of data analysis to make an exploratory analysis of data 
 * Make a process of data quality.

 Below you will find the notebooks used as guide into the class.

Contenido
-----------

## Unidad 1

**Temas**

 - [Repaso de python](https://nbviewer.org/urls/gitlab.com/udea1/data-analytic/raw/master/Repaso_de_python.ipynb)
 - [Tipos de datos](https://nbviewer.jupyter.org/urls/gitlab.com/udea1/data-analytic/raw/master/Tipos_de_datos.ipynb)
 - [Numpy básico](https://nbviewer.jupyter.org/urls/gitlab.com/udea1/data-analytic/raw/master/Numpy_basico.ipynb)
 - [Programación Orientada a Objetos en python](https://nbviewer.jupyter.org/urls/gitlab.com/udea1/data-analytic/raw/master/Intro_OOP_Python.ipynb)
 - [Matplotlib-basico](https://nbviewer.jupyter.org/urls/gitlab.com/udea1/data-analytic/-/raw/master/Matplotlib_basico.ipynb?flush_cache=true)
 - [Ejemplos de aplicacion](https://nbviewer.jupyter.org/urls/gitlab.com/udea1/data-analytic/-/raw/master/Ejemplos_aplicacion_python.ipynb)

**Tarea**

 - [Tarea1-FundamentosPython](https://nbviewer.jupyter.org/urls/gitlab.com/udea1/data-analytic/-/raw/master/homeworks/Tarea1-FundamentosPython.ipynb) 

## Unidad 2

**Temas**

 - [Tipo de formatos de datos](https://nbviewer.jupyter.org/urls/gitlab.com/udea1/data-analytic/-/raw/master/2.1_Tipos_de_formatos_datos.ipynb)
 - [Lectura/escritura de datos estructurados](https://nbviewer.jupyter.org/urls/gitlab.com/udea1/data-analytic/-/raw/master/2.2_Lectura_de_datos_1.ipynb)
 - [SQL básico](https://nbviewer.jupyter.org/urls/gitlab.com/udea1/data-analytic/-/raw/master/2.3_SQL_Basico.ipynb)
